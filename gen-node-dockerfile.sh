#!/bin/bash
#生成node项目dockerfile
# https://gitlab.com/api/v4/projects/35419017/repository/files/gen-node-dockerfile.sh/raw?ref=main
#curl https://gitlab.com/api/v4/projects/35419017/repository/files/gen-node-dockerfile.sh/raw?ref=main > gen-node-dockerfile.sh && chmod +x gen-node-dockerfile.sh && sh gen-node-dockerfile.sh
#dockerfile文件名
#DOCKER_FILE_NAME="Dockerfile"
#项目名
#CI_PROJECT_NAME="minio"
# -f 参数判断 $file 是否存在
if [ -f "$DOCKER_FILE_NAME" ]; then

echo '文件已存在';

else

echo '文件不存在';
cat > $DOCKER_FILE_NAME << EOF 
FROM nginx
RUN rm /etc/nginx/conf.d/default.conf
ADD default.conf /etc/nginx/conf.d/
WORKDIR /home/${CI_PROJECT_NAME}
COPY $CI_PROJECT_NAME /home/${CI_PROJECT_NAME}/
EOF

fi
