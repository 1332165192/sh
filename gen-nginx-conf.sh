#!/bin/bash
#生成nginx配置文件
# https://gitlab.com/api/v4/projects/35419017/repository/files/gen-nginx-conf.sh/raw?ref=main
# curl https://gitlab.com/api/v4/projects/35419017/repository/files/gen-nginx-conf.sh/raw?ref=main > gen-nginx-conf.sh && chmod +x gen-nginx-conf.sh && sh gen-nginx-conf.sh 
#nginx配置文件名
NGINX_CONF_FILE_NAME="default.conf"
#项目名
CI_PROJECT_NAME="minio"
# -f 参数判断 $file 是否存在
if [ -f "$NGINX_CONF_FILE_NAME" ]; then

echo '文件已存在';

else

echo '文件不存在';
cat > $NGINX_CONF_FILE_NAME << EOF 
server {
    listen       80;
    server_name  localhost;
    #前端-pc
    location /{
        root /home/${CI_PROJECT_NAME}/;
        index index.html;
    }
}
EOF

fi
