#!/bin/bash
#shell判断变量是否为空
para1=""
if [ ! $para1 ]; then
  echo "IS NULL"
  para1="1"
else
  echo "NOT NULL"
  para1="2"
fi

echo $para1

#shell判断文件,目录是否存在或者具有权限
file="settings.xml"
# -f 参数判断 $file 是否存在
if [ ! -f "$file" ]; then
  echo '文件不存在';
else
  echo '文件已存在';
fi

#比较字符串是否相等
CI_COMMIT_BRANCH=dev
if [ "$CI_COMMIT_BRANCH" = "release" ];then
    echo kubectl config use-context 1
else
    echo kubectl config use-context 2
fi
